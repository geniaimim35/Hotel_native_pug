//import * as $ from 'jquery' npm i -S jquery -подключение библиотеки
// import React from "react"; - подключение react
// import { render } from "react-dom"; - подключение react
// import "@/babel.js";
import "@/assets/styles/style.css";
import "@/assets/styles/main.scss";
import Head from "./js/header";

import Calendar from "./js/cards/calendar";

import DropDown from "./js/form/dropDown";

new Head();
new Calendar();
new DropDown();

